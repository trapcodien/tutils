#include <unistd.h>
#include <iostream>
#include <vector>
#include <thread>

#include "Guard.hpp"

using tutils::Guard;

Guard<int>		g(10);

void f(int i)
{
	while (true)
	{
		g([&](int & j){
			std::cout << "On passe ici et apres ca bloque :D - " << j << std::endl;
			g([&i](int & k){
				k = i;
				std::cout << "Hello World : " << i << std::endl;
				(void)k;
			});

			(void)j;
		});
	}
	(void)i;
}

int		main(void)
{
	std::vector<std::thread>	threads;

	for (int i = 0 ; i < 10; ++i)
		threads.push_back(std::move(std::thread(&f, i)));

	for (auto it = threads.begin(); it != threads.end(); it++)
		it->join();	
	return (0);
}
