# ifndef GUARD_HPP
# define GUARD_HPP

# include <iostream>
# include <thread>
# include <atomic>
# include <unordered_map>

namespace tutils
{
	template <typename T>
	class Guard
	{
	private :
		typedef std::unordered_map<std::thread::id, bool>		LockersMap;

		T						_content;
		std::atomic_flag		_flag = ATOMIC_FLAG_INIT;
		LockersMap				_lockers;

	public :
		Guard<T>(T && content) noexcept : _content(std::move(content)) {};
		Guard<T>(void) noexcept {};
	
		void	operator()(std::function<void(T & content)> f) noexcept
		{
			bool	first_time = false;
			auto id = std::this_thread::get_id();
			auto it = this->_lockers.find(id);
			if (it == this->_lockers.end())
				first_time = true;
			
			if (!first_time && it->second)
				return f(this->_content);

			while(this->_flag.test_and_set(std::memory_order_acquire));
			this->_lockers[id] = true;
			f(this->_content);
			this->_lockers[id] = false;
			this->_flag.clear(std::memory_order_release);
		}

	};
} /* namespace tutils */

#endif /* !GUARD_HPP */
